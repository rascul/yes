use std::env::args;
use std::error::Error;
use std::io::{BufWriter, Write, stdout};

fn main() -> Result<(), Box<dyn Error>> {
	let phrase = [args().nth(1).unwrap_or("y".into()), "\n".into()].concat();
	let phrase = phrase.as_bytes();

	let mut out = vec![phrase; 1024];
	out.fill(phrase);
	let out = out.concat();

	let mut buf = BufWriter::new(stdout());
	
	loop {
		buf.write(&out)?;
	}
}
